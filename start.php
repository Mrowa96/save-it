<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Save it!</title>  
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <script src="js/global.js"></script>
        <script src="js/tinymce/tinymce.min.js"></script>
        <script src="js/engine.js"></script>
        <script>
            window.addEventListener("load",function(){
                var send = document.getElementById("send"),
                    tableName = null,
                    xmlhttp = new XMLHttpRequest();
                
                send.addEventListener("click",function(){
                    
                    createTable();
                },false)
                
                document.addEventListener("keyup",function(e){
                    tableName = document.getElementById("tableName").value;
                   
                    if(e.keyCode == 13){
                        createTable(tableName);
                    }
                },false)
                
                function createTable(tn){
                    tn ? tableName = tn: tableName = document.getElementById("tableName").value;
                    
                    xmlhttp.open("POST","server/functions.php",true);
                    xmlhttp.onreadystatechange = function(){
                        if(xmlhttp.status == 200 && xmlhttp.readyState == 4){
                            if(xmlhttp.responseText !== ""){
                               window.location.assign("index.php?"+xmlhttp.responseText);
                            }
                        }
                    }
                    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                    xmlhttp.send("start=true&tableName="+tableName); 
                }
                
            },false)
        </script>
    </head>
    <body>
        <header>
            <span class="title">Save it!</span>
        </header>
        
        <div class="main_content start_screen">
            <form class="">
                <div class="">
                    <input type="text" id="tableName" placeholder="Wprowadź nazwę tabeli...">
                </div>
                
                <div class="">
                    <div id="send">Wyślij</div>
                </div>
                
            </form>
        </div>

        <footer>
            <span class="text">&copy; Copyright Paweł Mrowiec. All rights reserved.</span>
        </footer>
    </body>
</html>