<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Save it!</title>  
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <script src="js/global.js"></script>
        <script src="js/tinymce/tinymce.min.js"></script>
        <script src="js/engine.js"></script>
        <script src="js/main.js"></script>
    </head>
    <body>
        <header>
            <span class="title">Save it!</span>
            <a href="/start.php"><i class="fa fa-home"></i></a>
            <i id="create_note" class="fa fa-plus-circle"></i>
        </header>
        
        <div class="main content">
            
        </div>
        
        <div class="editor_panel">
            <header>
                Edytuj notatkę
                <i class="fa fa-floppy-o save"></i>
                <i class="fa fa-times close"></i>
            </header>
            <div>
                <textarea></textarea>
            </div>
            
        </div>
        
        <footer>
            <span class="text">&copy; Copyright Paweł Mrowiec. All rights reserved.</span>
            <span class="info">
                Utworzonych:<span class="created">0</span> 
                Wyświetlanych: <span class="displayed">0</span>
            </span>
        </footer>
    </body>
</html>