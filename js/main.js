window.addEventListener("load",function(e){
    var create_note = document.getElementById("create_note"),       
        notes_arr = [],
        xmlhttp = new XMLHttpRequest(),
        table_name =  window.location.href.split("?")[1]

    xmlhttp.open("POST","server/functions.php",true);
    xmlhttp.onreadystatechange = function(){
        if(xmlhttp.status == 200 && xmlhttp.readyState == 4){
            if(xmlhttp.responseText !== ""){
                var data = JSON.parse(xmlhttp.responseText);
                for(var i=0; i<data.length;i++){
                    create(data[i]);
                }
            }
        }
    }
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("tableName="+table_name); 

    document.addEventListener("dragstart",function(e){
        e.preventDefault();
    },false)
    
    global.notesArray = notes_arr;
    
    tinymce.init({
        selector: "textarea",
        
        toolbar1: "bold italic underline strikethrough | fontsizeselect | bullist numlist | forecolor",

        menubar: false,
        toolbar_items_size: 'small',
        height: window.innerHeight,

        style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
     });
    
    create_note.addEventListener("click",function(){
        create();
    })
    
    function create(data){
        var tmp_note = null;

        tmp_note = new _note();
        tmp_note.create(data);
        notes_arr.push(tmp_note);
        global.noteIndex++;
        global.noteIndexRef++;
        
        global.created.innerHTML = global.noteIndex;
        global.displayed.innerHTML = global.noteIndexRef;
    }

},false);

