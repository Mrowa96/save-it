function _note(){
    var that = {};
    that.defaultText = "Note",
    that.mouseDown = {x:null,y:null,on:false,select:false},
    that.readyToResize = false,
    that.note = null,
    that.noteAttributes = null,
    that.resizeing = false,
    that.body = null,
    that.noteIndex = global.noteIndex,
    that.headerColor = null,
    that.mouse = new _mouse(),

    that.create = function(data){
        that.note = document.createElement("div");
        data ? that.note.id = data.id : that.note.id = null;
        data ? that.note.classList.add("note","id_"+that.note.id) :  that.note.classList.add("note");
        data ? that.note.style.top = data.y+"px" : that.note.style.top = global.pageHeaderAttribbutes.height+"px";
        data ? that.note.style.left = data.x+"px" : that.note.style.left = "0px";
        data ? that.note.style.zIndex = data.zIndex : that.note.style.zIndex = that.handleIndex();
        data ? that.note.style.width = data.width+"px" : that.note.style.width = "152px";
        data ? that.note.style.height = data.height+"px" : that.note.style.height = "152px";
        data ? that.fromDB = true : that.fromDB = false;
        
        global.body.appendChild(that.note);
        that.createHeader(data);
        that.createBody(data);
        
        if(!that.fromDB){
            var xmlhttp = new XMLHttpRequest();
            
            xmlhttp.open("POST","server/functions.php",true);
            xmlhttp.onreadystatechange = function(){
                if(xmlhttp.status == 200 && xmlhttp.readyState == 4){
                    console.log(xmlhttp.responseText)
                    that.note.id = xmlhttp.responseText;
                    that.note.classList.add("id_"+xmlhttp.responseText)
                }
            }
            xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xmlhttp.send("create=true&color="+that.headerColor+"&tableName="+global.tableName+"&x="+parseInt(that.note.style.left)+"&y="+parseInt(that.note.style.top)+"&zIndex="+that.note.style.zIndex);
        }

        that.note.addEventListener("mousedown",function(e){
            var e = e || window.event

            global.currentNote = that.note;
            that.mouse.handle(e);
            that.mouseDown.on = true;
            
            if(that.fromDB){
                that.mouseDown.x = that.mouse.x-that.noteAttributes.left+parseInt(that.note.style.left);
                that.mouseDown.y = that.mouse.y-that.noteAttributes.top+parseInt(that.note.style.top)-40;
            }
            else{
                that.mouseDown.x = that.mouse.x-that.noteAttributes.left;
                that.mouseDown.y = that.mouse.y-that.noteAttributes.top;
            }

            that.note.style.boxShadow = "0 0px 5px rgba("+that.headerColor+",0.75)";

            that.handleIndex("in");

            if((that.mouse.x>(that.noteAttributes.left+(that.noteAttributes.width-25)) && (that.mouse.y>(that.noteAttributes.top+(that.noteAttributes.height-25))))){
                that.resizeing = true;
            }
            
        },false)
        
        /*document.addEventListener("mousedown",function(z){
            var e = e || window.event;
            if(!e.target.classList.contains("note")){
                that.mouseDown.select = true;
            }
        },false);*/
        
        /*document.addEventListener("mouseup",function(){
            if(that.mouseDown.select){
                that.mouseDown.select = false;
            }
        },false);*/
        
        document.addEventListener("mousemove",function(e){
            var e = e || window.event;
            that.mouse.handle(e);
            that.noteAttributes = that.note.getBoundingClientRect();

            that.resize(that.mouse,global.pageHeaderAttribbutes);
            that.move(global.currentNote, global.pageHeaderAttribbutes)
            
        },false)
        
        that.note.addEventListener("mouseup",function(e){
            that.out();
            that.handleIndex("out");
            
            if(!e.target.classList.contains("delete_note") && !e.target.classList.contains("edit_note")){
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("POST","server/functions.php",true);
                xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xmlhttp.send("savePosition=true&noteId="+that.note.id+"&x="+parseInt(that.noteAttributes.left)+"&y="+parseInt(that.noteAttributes.top)+"&zIndex="+that.note.style.zIndex+"&height="+that.noteAttributes.height+"&width="+that.noteAttributes.width);
            }
            
        },false)
        
        that.note.addEventListener("mouseout",function(){
            if(!that.mouseDown.on){
                that.out();
            }
        },false)
    },
    
    that.select = function(mouse){
        that.selectingDiv = document.createElement("div");  
        
        
        global.body.appendChild(that.selectingDiv)
        console.log(mouse)
        that.selectingDiv.classList.add("select");
        that.selectingDiv.style.x = mouse.x+"px";
        that.selectingDiv.style.y = mouse.y+"px";
    }
    
    that.handleIndex = function(status){
        for(var i=0, zIndexArr = [];i<global.notesArray.length; i++){
           zIndexArr[i] = global.notesArray[i].note.style.zIndex;
        }
        if(isNaN(parseInt(that.note.style.zIndex))){
            var new_item = true;
        }
        if(new_item || parseInt(global.currentNote.style.zIndex) != Math.max.apply(Math,zIndexArr) ){
            if(status === "in" || new_item){
                if(new_item){
                    if(zIndexArr.length == 0){
                        return "1";
                    }
                    else{
                        that.note.style.zIndex = Math.max.apply(Math,zIndexArr)+1;
                    }
                    
                    
                    return ;
                }
                global.currentNote.style.zIndex = Math.max.apply(Math,zIndexArr)+2
            }
            else{
                global.currentNote.style.zIndex =  parseInt(global.currentNote.style.zIndex)-1
            }
        }
    },
        
    that.createHeader = function(data){
        var headerColors= ["244,67,54","103,58,183","63,81,181","0,188,212","139,175,74","255,193,7","255,87.34"],
            headerColor =  headerColors[Math.floor((Math.random() *headerColors.length )+0)],
            noteHeader = document.createElement("header"),
            noteHeaderText = document.createElement("span"),
            editNote = document.createElement("i"),
            deleteNote = document.createElement("i");

        data ? headerColor = data.color: headerColor = headerColor;
           
        that.headerColor = headerColor;
        noteHeader.classList.add("note_header");
        noteHeaderText.innerHTML = that.defaultText;
        that.note.appendChild(noteHeader);
        
        editNote.classList.add("fa","fa-pencil-square-o","edit_note");
        editNote.addEventListener("click",function(){
            var editor = new _editor();
            that.note.classList.add("edited")
            tinyMCE.activeEditor.setContent(that.body.innerHTML);

            if(!editor.showed){
                editor.show("slide left");
            }
            else{
                editor.hide("slide left");
            }
            
        },false);
        
        deleteNote.classList.add("fa","fa-times","delete_note");
        deleteNote.addEventListener("click",function(){
            global.body.removeChild(that.note)
            global.noteIndexRef--;
            global.displayed.innerHTML = global.noteIndexRef;
            
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("POST","server/functions.php",true);
            xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xmlhttp.send("delete=true&&noteId="+that.note.id);

        },false);

            noteHeader.appendChild(editNote);
            noteHeader.appendChild(noteHeaderText);
            noteHeader.appendChild(deleteNote);
            noteHeader.style.background = "rgb("+headerColor+")";
    },
        
    that.createBody = function(data){
        var body = document.createElement("article");
        
        body.classList.add("body");
        that.body = body;
        
        that.note.appendChild(body);
        
        data ? body.innerHTML = data.text : null;
    },
        
    that.resize = function(mouse,pageHeaderAttribbutes){
        if(that.resizeing){
            that.readyToResize = true;
            if(that.mouseDown.on){
                if(that.note.style.height !== "100px" && that.note.style.width !== "100px"){
                    that.note.style.height = (that.mouse.y-(that.noteAttributes.top-pageHeaderAttribbutes.height)-30+"px");
                    that.note.style.width = (that.mouse.x-(that.noteAttributes.left)+10+"px");
                }
                else{
                    that.out();
                    that.note.style.height = "101px";
                    that.note.style.width = "101px";
                }
            }
        }
        else{
            that.readyToResize = false;
        }
    },
    
    that.move = function(note,pageHeaderAttribbutes){
        if(that.mouseDown.on && !that.readyToResize){
            var vector_Y = (that.mouse.y-that.mouseDown.y-pageHeaderAttribbutes.height),
               vector_X = (that.mouse.x-that.mouseDown.x);
            
            note.style.transform = "translate("+vector_X+"px,"+vector_Y+"px)";
        }
    },
        
    that.out = function(){
        that.mouseDown.on = false;
        that.readyToResize = false;
        that.note.style.boxShadow = "0 3px 5px rgba(212,212,212,0.75)";
        that.resizeing = false;
    }
    
    return that;
}

function _editor(){
    var that = {};
    
    that.editorPanel = document.getElementsByClassName("editor_panel")[0];
    that.editorPanelClose = that.editorPanel.getElementsByClassName("close")[0];
    that.editorPanelSave = that.editorPanel.getElementsByClassName("save")[0];
    that.showed = false;
    
    that.editorPanelSave.addEventListener("click",function(){
        var cur = document.getElementsByClassName("edited")[0],
            noteId = cur.getAttribute("class").split("id_")[1]
        
        cur.getElementsByClassName("body")[0].innerHTML = tinyMCE.activeEditor.getContent();
        that.hide("slide right")
        cur.classList.remove("edited")
        
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST","server/functions.php",true);
        xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xmlhttp.send("save=true&&noteId="+noteId+"&text="+tinyMCE.activeEditor.getContent());
        
    },false);
    
    that.editorPanelClose.addEventListener("click",function(){
        var cur = document.getElementsByClassName("edited")[0];
        that.hide("slide right");
        cur.classList.remove("edited")
    },false);
    
    that.show = function(transform){
        var transform = transform.split(" "),
            translate = "";

        switch(transform[0]){
            case "slide":
                translate += "translate"
                break;
        }

        switch(transform[1]){
            case "left":
                translate += "X(0%)";
                break;
        }

        that.editorPanel.style.transform = translate;
        that.showed = true;
    }
    
    that.hide = function(transform,show){
        var transform = transform.split(" "),
            translate = "";

        switch(transform[0]){
            case "slide":
                translate += "translate"
                break;
        }

        switch(transform[1]){
            case "right":
                translate += "X(100%)";
                break;
        }

        that.editorPanel.style.transform = translate;
        that.showed = false;
        
        if(show){
            that.show("slide left");
        }
    }
    
    return that;
}

function _mouse(){
    var that = {};
    
    that.x = null,
    that.y = null,
        
    that.handle = function(e){
        if(e){
            that.x = e.clientX;
            that.y = e.clientY;    
        }
    },
    that.log = function(){
        document.addEventListener("mousemove",function(e){
            that.handle();
            console.log(that.x,that.y)
        },false)
    }
    
    return that;
}