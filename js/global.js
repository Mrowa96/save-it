var global = {};

window.addEventListener("load",function(){
    global = {
        currentNote: null,
        pageHeaderAttribbutes: document.getElementsByTagName("header")[0].getBoundingClientRect(),
        displayed: document.getElementsByClassName("displayed")[0],
        created: document.getElementsByClassName("created")[0],
        body: document.getElementsByTagName("body")[0],
        noteIndex: 0,
        noteIndexRef: 0,
        notesArray: null,
        hexToRgb : function(hex,str) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            if(str){
                return result ? parseInt(result[1], 16)+","+parseInt(result[2], 16)+","+parseInt(result[3], 16) :null;
            }
            else{
                return result ? {
                    r: parseInt(result[1], 16),
                    g: parseInt(result[2], 16),
                    b: parseInt(result[3], 16)
                } : null;
            }
        },
        tableName: window.location.search.substr(1,window.location.search.length-1)
    }
},false)