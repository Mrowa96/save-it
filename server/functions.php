<?php
    $conn = new mysqli("localhost", "root", "", "dragndrop");
    date_default_timezone_set("Europe/Warsaw");

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 

    if(isset($_POST['start'])){
        $tableName = $_POST['tableName'];
        
        $sql = "SELECT * FROM tables WHERE name='".$tableName."'";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                echo $row["name"];
            }
        } 
        else {
           $sql = "INSERT INTO tables (name,created) VALUES ('".$tableName."','".date('Y-m-d h:i:sa')."')";
            if ($conn->query($sql) !== true) {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }
    }
    elseif(isset($_POST['create'])){
        $color = $_POST['color'];
        $tableName = $_POST['tableName'];
        $x = $_POST['x'];
        $y = $_POST['y'];
        $zIndex = $_POST['zIndex'];
        
        $sql = "INSERT INTO data (color,table_name,x,y,zIndex) VALUES('".$color."','".$tableName."','".$x."','".$y."','".$zIndex."')";
        $conn->query($sql);
        
        echo $conn->insert_id;
        
    }
    elseif(isset($_POST['save'])){
        $id = $_POST['noteId'];
        $text = $_POST['text'];
        $sql = "UPDATE data SET text='".$text."' WHERE id='".$id."'";
        if ($conn->query($sql) !== true) {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    elseif(isset($_POST['savePosition'])){
        $id = $_POST['noteId'];
        $x = $_POST['x'];
        $y = $_POST['y'];
        $width = $_POST['width'];
        $height = $_POST['height'];
        $zIndex = $_POST['zIndex'];

        $sql = "UPDATE data SET x='".$x."', y='".$y."',zIndex='".$zIndex."', width='".$width."', height='".$height."' WHERE id='".$id."'";
        if ($conn->query($sql) !== true) {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        else{
            echo "ok pos";
        }
    }
    elseif(isset($_POST['delete'])){
        $id = $_POST['noteId'];
        
        $sql = "DELETE FROM data WHERE id='".$id."'";
        if ($conn->query($sql) !== true) {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    elseif(isset($_POST['tableName'])){
        $tableName = $_POST['tableName'];
        $sql = "SELECT * FROM data WHERE table_name='".$tableName."'";
        $result = $conn->query($sql);
        
        
        if ($result->num_rows > 0) {
            $json = "[";
            while($row = $result->fetch_assoc()) {
                $json .= '{"id":"'.$row['id'].'","x":"'.$row['x'].'","y":"'.$row['y'].'","width":"'.$row['width'].'","height":"'.$row['height'].'", "zIndex":"'.$row['zIndex'].'","color":"'.$row['color'].'","text":"'.$row['text'].'"},';
            }
            $json = substr($json,0,mb_strlen($json)-1);
            $json .= "]";
        }
        else{
            $json = '{"":""}';
        }
        
        
        echo $json;
        
    }

?>